// Copyright © 2015 Rodolphe Cargnello, rodolphe.cargnello@gmail.com
// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <vector>
#include <string>
#include <typeinfo>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/literal/_s.hpp>


int main()
{
	std::cout << "Test #include <hopp/literal/_s.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	nb_test++;
	{
		auto s = "A string"_s;
		
		std::cout << "Type info of \"A string\" = " << typeid("A string").name() << std::endl;
		std::cout << "Type info of std::string = " << typeid(std::string).name() << std::endl;
		std::cout << "Type info of s (\"" << s << "\") = " << typeid(s).name() << std::endl;
		
		nb_test -= hopp::test(std::is_same<decltype(s), std::string>::value, "hopp/literal/_s fails\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp/literal/_s: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}

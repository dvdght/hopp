// Copyright © 2012, 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/algo/sum.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/algo/sum.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	++nb_test;
	{
		std::vector<int> v = { 21, 73, 42 };
		auto const sum = hopp::sum(v);
		
		std::cout << "Sum of " << v << " = " << sum << std::endl;
		
		nb_test -= hopp::test(sum == 136, "hopp::sum fails\n");
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<std::string> v = { "Concatenate ", "some ", "strings" };
		auto const sum = hopp::sum(v);
		
		std::cout << "Sum of " << v << " = " << sum << std::endl;
		
		nb_test -= hopp::test(sum == "Concatenate some strings", "hopp::sum fails\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::sum: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}

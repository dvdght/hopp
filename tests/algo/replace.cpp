// Copyright © 2013, 2015 Lénaïc Bagnères, hnc@singularity.fr

// This file is part of hopp.

// hopp is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.

// hopp is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with hopp. If not, see <http://www.gnu.org/licenses/>


#include <iostream>
#include <vector>
#include <list>
#include <string>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/algo/replace.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/algo/replace.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	// Empty
	
	++nb_test;
	{
		std::vector<int> c(0);
		hopp::replace(c, c, c);
		nb_test -= hopp::test
		(
			c == std::vector<int>(0),
			"hopp::replace in an empty vector fails\n"
		);
	}
	
	++nb_test;
	{
		std::list<int> c(0);
		hopp::replace(c, c, c);
		nb_test -= hopp::test
		(
			c == std::list<int>(0),
			"hopp::replace in an empty list fails\n"
		);
	}
	
	std::cout << "Common cases\n" << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':'}, std::vector<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {' '}, {' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':'}, std::vector<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {' '}, {'_'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', '_', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Common cases with std::list\n" << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':'}, std::list<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {' '}, {' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':'}, std::list<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {' '}, {'_'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', '_', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Begin & End\n" << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {'t', 'e', 's', 't', ' '}, std::vector<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}, std::vector<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({' ', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' '});
		std::vector<char> r = c;
		hopp::replace(r, {' '}, {'_'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'_', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' '}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({' ', ' ', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' ', ' '});
		std::vector<char> r = c;
		hopp::replace(r, {' ', ' '}, {'_', '_'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'_', '_', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' ', ' '}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Begin & End with std::list\n" << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {'t', 'e', 's', 't', ' '}, std::list<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}, std::list<char>(0));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({' ', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' '});
		std::list<char> r = c;
		hopp::replace(r, {' '}, {'_'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'_', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' '}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({' ', ' ', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' ', ' '});
		std::list<char> r = c;
		hopp::replace(r, {' ', ' '}, {'_', '_'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'_', '_', 't', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e', ' ', ' '}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Same value\n" << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {'t', 'e', 's', 't'}, {'t', 'e', 's', 't'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {'r', 'e', 'p', 'l', 'a', 'c', 'e'}, {'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {'h', 'n', 'c'}, {'h', 'n', 'c'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Same value with std::list\n" << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {'t', 'e', 's', 't'}, {'t', 'e', 's', 't'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {'r', 'e', 'p', 'l', 'a', 'c', 'e'}, {'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {'h', 'n', 'c'}, {'h', 'n', 'c'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Consecutive\n" << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::vector<char> r = c;
		hopp::replace(r, {'t', 't'}, {'t'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 'e', 's', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::vector<char> r = c;
		hopp::replace(r, {'e', 'e'}, {'e'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 't', 'e', 's', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::vector<char> r = c;
		hopp::replace(r, {'s', 's'}, {'s'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 't', 'e', 'e', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::vector<char> r = c;
		hopp::replace
		(
			hopp::replace
			(
				hopp::replace
				(
					r,
					{'t', 't'}, {'t'}
				),
				{'e', 'e'}, {'e'}
			),
			{'s', 's'}, {'s'}
		);
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Consecutive with std::list\n" << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::list<char> r = c;
		hopp::replace(r, {'t', 't'}, {'t'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 'e', 's', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::list<char> r = c;
		hopp::replace(r, {'e', 'e'}, {'e'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 't', 'e', 's', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::list<char> r = c;
		hopp::replace(r, {'s', 's'}, {'s'});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 't', 'e', 'e', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;

	++nb_test;
	{
		std::list<char> c({'t', 't', 'e', 'e', 's', 's', 't', 't'});
		std::list<char> r = c;
		hopp::replace
		(
			hopp::replace
			(
				hopp::replace
				(
					r,
					{'t', 't'}, {'t'}
				),
				{'e', 'e'}, {'e'}
			),
			{'s', 's'}, {'s'}
		);
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', 't'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "std::string\n" << std::endl;
	
	++nb_test;
	{
		std::string c = "a std::string";
		std::string r = c;
		hopp::replace(r, std::string("a "), std::string("works with "));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == "works with std::string",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Complete container\n" << std::endl;
	
	++nb_test;
	{
		std::string c = "replace all";
		std::string r = c;
		hopp::replace(r, std::string("replace all"), std::string("all replaced"));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == "all replaced",
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "const std::string\n" << std::endl;
	
	++nb_test;
	{
		std::string const c = "a std::string";
		std::string r = hopp::replace_copy(c, std::string("a "), std::string("works with "));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == "works with std::string",
			"hopp::replace_all fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "const Complete container\n" << std::endl;
	
	++nb_test;
	{
		std::string const c = "replace all";
		std::string r = hopp::replace_copy(c, std::string("replace all"), std::string("all replaced"));
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == "all replaced",
			"hopp::replace_all fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Iterator\n" << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, r.begin() + 4, r.begin() + 15, {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, r.begin() + 5, r.begin() + 15, {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::vector<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::vector<char> r = c;
		hopp::replace(r, r.begin() + 4, r.begin() + 14, {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	
	std::cout << "Iterator with std::list\n" << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, std::next(r.begin(), 4), std::next(r.begin(), 15), {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, std::next(r.begin(), 5), std::next(r.begin(), 15), {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	++nb_test;
	{
		std::list<char> c({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'});
		std::list<char> r = c;
		hopp::replace(r, std::next(r.begin(), 4), std::next(r.begin(), 14), {' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' '}, {' '});
		std::cout << c << "\n" << r << std::endl;
		nb_test -= hopp::test
		(
			r == decltype(r)({'t', 'e', 's', 't', ' ', 't', 'o', ' ', 'd', 'e', 'l', 'e', 't', 'e', ' ', 'h', 'n', 'c', ':', ':', 'a', 'l', 'g', 'o', ':', ':', 'r', 'e', 'p', 'l', 'a', 'c', 'e'}),
			"hopp::replace fails\n"
		);
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::replace: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}

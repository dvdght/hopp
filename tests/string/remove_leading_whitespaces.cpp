// Copyright © 2015 Lénaïc Bagnères, hnc@singularity.fr

// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


#include <iostream>
#include <string>
#include <vector>
#include <list>

#include <hopp/test.hpp>
#include <hopp/conversion/to_string.hpp>
#include <hopp/string/remove_leading_whitespaces.hpp>
#include <hopp/print/std.hpp>


int main()
{
	std::cout << "Test #include <hopp/string/remove_leading_whitespaces.hpp>" << std::endl;
	std::cout << std::endl;
	
	int nb_test = 0;
	
	nb_test++;
	{
		std::string s = "   \t   \t\t A std::string";
		
		std::cout << "Before hopp::remove_leading_whitespaces =" << std::endl;
		std::cout << s << std::endl;
		std::cout << std::endl;
		
		hopp::remove_leading_whitespaces(s);
		
		std::cout << "After =" << std::endl;
		std::cout << s << std::endl; // "A std::string"
		
		nb_test -= hopp::test(s == "A std::string", "hopp::remove_leading_whitespaces fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::list<char> s;
		for (char const c : " \t   \t\t A std::list")
		{
			if (c != '\0') { s.push_back(c); }
		}
		
		std::cout << "Before hopp::remove_leading_whitespaces =" << std::endl;
		std::cout << s << std::endl;
		std::cout << std::endl;
		
		hopp::remove_leading_whitespaces(s);
		
		std::cout << "After =" << std::endl;
		std::cout << s << std::endl;
		
		nb_test -= hopp::test(s.size() == 11, "hopp::remove_leading_whitespaces fails\n");
	}
	std::cout << std::endl;
	
	nb_test++;
	{
		std::vector<char> s;
		for (char const c : " \t   \t\t A std::vector")
		{
			if (c != '\0') { s.push_back(c); }
		}
		
		std::cout << "Before hopp::remove_leading_whitespaces =" << std::endl;
		std::cout << s << std::endl;
		std::cout << std::endl;
		
		hopp::remove_leading_whitespaces(s);
		
		std::cout << "After =" << std::endl;
		std::cout << s << std::endl;
		
		nb_test -= hopp::test(s.size() == 13, "hopp::remove_leading_whitespaces fails\n");
	}
	std::cout << std::endl;
	
	hopp::test(nb_test == 0, "hopp::remove_leading_whitespaces: " + hopp::to_string(nb_test) + " test(s) fail(s)!\n");
	
	return nb_test;
}
